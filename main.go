package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

var (
	token string
	id_list chan int
	resolution int
)

func init() {
	id_list = make(chan int, 1)

	tokenFile := os.Args[1]

	rawToken, err := os.ReadFile(tokenFile)
	token = strings.TrimSpace(string(rawToken))

	if err != nil {
		log.Fatalf("Could not read token from file %s: %s", tokenFile, err)
		panic(err)
	}

	var compDir string

	flag.StringVar(&compDir, "compdir", ".", "The directory used for compilation.")
	flag.IntVar(&resolution, "res", 1000, "The resolution of the to-be-generated PNGs.")

	flag.Parse()

	os.Chdir(compDir)
}

func main() {
	discord, err := discordgo.New("Bot " + token)

	if err != nil {
		log.Fatalf("Could not connect to Discord: %S", err)
		panic(err)
	} else {
		log.Println("Connected to Discord!")
	}


	go func() {
		i := 0;
		for {
			id_list<-i
			i++
		}
	}()

	discord.AddHandler(messageHandler)

	discord.Identify.Intents = discordgo.IntentsGuildMessages | discordgo.IntentsMessageContent
	if err := discord.Open(); err != nil {
		log.Fatalf("An error occurred opening the discord client: %s", err)
		panic(err)
	}

	fmt.Println("Press Ctrl+C to close...")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	discord.Close()
}

func trimAll(s string, prefixes []string, other []string) string {
	s = strings.TrimSpace(s)
	for i := 0; i < len(prefixes); i++ {
		s = strings.TrimSpace(strings.TrimPrefix(s, prefixes[i]))
	}
	for i := 0; i < len(other); i++ {
		s = strings.TrimSpace(strings.Trim(s, other[i]))
	}
	return s
}

func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	ref := m.Reference()
	if !strings.HasPrefix(m.Content, "latex!") {
		return
	}
	log.Printf("Latex message from %s", m.Author.Username)

	data := trimAll(m.Content, []string{"latex!", "```tex", "`"}, []string{"`"})

	if !strings.Contains(data, "\\begin{document}") {
		data = "\\documentclass[12pt]{article}\n\\usepackage{nopageno}\n\\begin{document}" + data + "\n\\end{document}"
	} else if !strings.Contains(data, "\\usepackage{nopageno}") {
		data = strings.Replace(data, "\\begin{document}", "\\usepackage{nopageno}\n\\begin{document}", 1)
	}

	id := <- id_list
	id_str := fmt.Sprint(id)

	if err := os.WriteFile(id_str + ".tex", []byte(data), 0666); err != nil {
		log.Fatalf("An error ocurred while trying to write to file %s: %s", id_str, err)
		panic(err)
	}

	defer os.Remove(id_str + ".tex")

	log.Printf("Written .tex file for message from %s", m.Author.Username)

	messageRaw, errored := runCommand("pdflatex", id_str + ".tex", "-no-shell-escape")

	defer os.Remove(id_str + ".aux")
	defer os.Remove(id_str + ".log")

	if errored {
		message := ""
		messageRawLines := strings.Split(messageRaw, "\n")
		add := 0
		for i := 0; i < len(messageRawLines); i++ {
			if add == 0 && strings.HasPrefix(messageRawLines[i], "!") {
				add = 1
				message += messageRawLines[i] + "\n"
			} else if add == 1  {
				if !strings.Contains(messageRawLines[i], "<inserted text>") {
					message += messageRawLines[i] + "\n"
				} else {
					add = 2
				}
			}

		}

		message = "An error ocurred while processing LaTeX. Stdout of `pdflatex`:\n```\n" + message + "```"

		s.ChannelMessageSendReply(m.ChannelID, message, ref)
		return
	}

	log.Printf("Run pdflatex on .tex file for message from %s", m.Author.Username)

	message, errored := runCommand("pdfcrop", id_str + ".pdf")

	if errored {
		s.ChannelMessageSendReply(m.ChannelID, message, ref)
		return
	}

	defer os.Remove(id_str + ".pdf")

	log.Printf("Run pdfcrop on .pdf file for message from %s", m.Author.Username)

	message, errored = runCommand("pdftoppm", "-png", "-r", fmt.Sprint(resolution), id_str + "-crop.pdf", id_str)

	if errored {
		s.ChannelMessageSendReply(m.ChannelID, message, ref)
		return
	}

	defer os.Remove(id_str + "-crop.pdf")

	log.Printf("Run pdftoppm on -crop.pdf file for message from %s", m.Author.Username)

	file, err := os.Open(id_str + "-1.png")
	if err != nil {
		log.Fatalf("Could not read file %s: %s!", id_str + "-1.png", err)
		panic(err)
	}

	log.Printf("Opened .png file for message from %s", m.Author.Username)

	_, err = s.ChannelFileSendWithMessage(m.ChannelID, "<@" + m.Author.ID + ">, your LaTeX has rendered.", id_str + ".png", file)
	if err != nil {
		panic(err)
	}

	log.Printf("Sent .png file for message from %s", m.Author.Username)

	file.Close()

	os.Remove(id_str + "-1.png")
}

func runCommand(binary string, args...string) (msg string, errored bool) {

	cmd := exec.Command(binary, args...)

	if output, err := cmd.Output(); err != nil {
		switch e := err.(type) {
		case *exec.ExitError:
			message := "An error ocurred while generating LaTeX. Stdout of `" + binary + "`:\n```" + string(output) + "``` `Stderr`:\n``` " + string(e.Stderr) + "```\n"
			return message, true
		default:
			log.Fatalf("An unknown error ocurred while generating pdf: %s", err)
			panic(err)
		}
	}

	return "", false
}
