# Lategoer

A discord bot that processes LaTeX and sends it, written in go.

It can process both inline and full-document LaTeX. 

## Examples

![Inline image](./examples/1.png)

![Full document image](./examples/2.png)

## Quick Start
``` shell
$ git clone git@gitlab.com:daisyflare/lategoer
$ cd lategoer
$ go build
$ ./lategoer <token file>
```

Depends upon pdflatex, pdfcrop, and pdftoppm.

